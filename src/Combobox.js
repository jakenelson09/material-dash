import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';


export default function ComboBox() {
    return (
      <Autocomplete
        id="combo-box-demo"
        options={diagnosisTaxonomy}
        getOptionLabel={(option) => option.diagnosis}
        style={{ width: 300 }}
        renderInput={(params) => <TextField {...params} label="Select Diagnosis" variant="outlined" />}
      />
    );
  }
  
  // List of diagnoses 
  const diagnosisTaxonomy = [
    { diagnosis: 'Melanoma', path: 'melanomapath' },
    { diagnosis: 'Squamous cell carcinoma', path: 'sccpath' },
    { diagnosis: 'Basal cell carcinoma', path: 'bccpath' },
   
  ];
  