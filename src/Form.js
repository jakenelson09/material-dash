import React from 'react';
import Combobox from './Combobox';
import Checkbox from '@material-ui/core/Checkbox'
import TextField from '@material-ui/core/TextField';
import DropzoneAreaExample from './Dropzone'
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function Form() {

  return (
    <React.Fragment>
        <span className='form-label'>Search for diagnosis here:</span>
        <Combobox /><br/>
        <form noValidate autoComplete="off">
        <TextField id="outlined-basic" label="Notes" variant="outlined" />
        </form><br/>
        <FormControlLabel
          value="end"
          control={<Checkbox color="primary" />}
          label="Contains PHI"
          labelPlacement="end"
        /><br/>
        <DropzoneAreaExample className='dropzone-class'/><br/>
    </React.Fragment>
  );
}
